﻿--解決SmarterASP 預設定序又有 權限不足的問題!!
ALTER DATABASE DB_9AB840_Vague SET SINGLE_USER WITH ROLLBACK IMMEDIATE
ALTER DATABASE DB_9AB840_Vague COLLATE Chinese_Taiwan_Stroke_CI_AS
ALTER DATABASE DB_9AB840_Vague SET MULTI_USER 
--單一某個欄位變更定序的問題
ALTER TABLE dbo.Stock  ALTER COLUMN id NVARCHAR(4) COLLATE Chinese_Taiwan_Stroke_CI_AS
ALTER TABLE dbo.StockParas  ALTER COLUMN ma_dscr NVARCHAR(50) COLLATE Chinese_Taiwan_Stroke_CI_AS
--
exec [dbo].[sp_upateLatestCloseDate]
exec [dbo].[sp_setKbar] '2020-08-28 00:00:00'

select * from stockParas where closeDate='2020-08-28 00:00:00' 
select * from stock where id=2241
insert into stock(id,name) values ('2241','艾姆勒');

--刪除重複的筆數
delete from stockParas where id in (
	select max(id) from stockParas where closeDate ='2020-08-28 00:00:00' group by stockId
);

--檢查使用了多少檔案空間?
SELECT file_id, name, type_desc, size, max_size  FROM sys.database_files where type_desc='ROWs';  
/*
1	DB_9AB840_Vague_Data	ROWS	7808	768000
1	DB_9AB840_Vague_Data	ROWS	6296	768000
1	DB_9AB840_Vague_Data	ROWS	6712	768000
1	DB_9AB840_Vague_Data	ROWS	5920	768000
1	DB_9AB840_Vague_Data	ROWS	5800	768000
1	DB_9AB840_Vague_Data	ROWS	5728	768000
1	DB_9AB840_Vague_Data	ROWS	5696	768000
1	DB_9AB840_Vague_Data	ROWS	5376	768000
1	DB_9AB840_Vague_Data	ROWS	5504	768000
*/

sp_helptext vVolumn
sp_helptext vLatestVolumn  --今天最新的三大法人
sp_helptext [vStock]
sp_helptext vVolumnTop50Fc  -- select*from vVolumnTop50Fc where maxFc > avcFc order by sumfc desc    /* 三大法人-外資前四十大 */
sp_helptext [sp_setKbar]
sp_helptext [sp_upateLatestCloseDate]

select * from tag
select substring(replace(newid(),'-',''),1,8)
insert into Tag (stockId,tag,createdDate) values (4106,'防疫',getdate());

--找出關鍵點位真的有壓過去的訊號
update [criticalPt] set isTouch=null,crossDate=null where stockid=1526;
select * from [dbo].[criticalPt] order by stockId,v desc
select * from [dbo].[criticalPt] where stockID=1526 order by v desc
select * from vStock where closeDate='2020-08-21' 
		--and criticalPt is not null
		and predict='近期短噴'
		select distinct box_msg from vStock

select * from vCriticalTouched
select * from vStock

update a set 
	a.isTouch=1,
	a.crossDate=b.closeDate
from criticalPt as a 
join vStock as b on a.stockId = b.stockId
where 1=1
--and b.stockId=1526 
and closeDate='2020-08-21'
and a.v >= b.low and a.v <= b.high
;

Alter view vCriticalTouched
as
select 
a.stockId,
a.critical,
a.v,
a.isTouch,
convert(varchar(20),a.critiDate,120) as critiDate,
convert(varchar(20),a.createdDate,120) as createdDate,
a.StopLoss,
a.memo

,b.stockName
,b.tag as stockTag
,b.isFavor
,b.iGap
,b.iSmGap
,b.c
,b.o
,b.low
,b.high
,b.v as stockV
from criticalPt as a 
join vStock as b on a.stockId = b.stockId
where 1=1
--and b.stockId=1526 
and closeDate in (select top 1 closeDate from stockLastCloseDate order by closeDate desc)
and a.v >= b.low and a.v <= b.high and b.kbar='紅K'
--end


select * from [dbo].[stockLastCloseDate] order by closeDate desc





--============================================
select * from stock where type is not null;
select top 100 * from vStock 
select * from criticalPt 
select * from tag 

--匯出CSV for YesWin
select stockId from 
(
	select 
	rank() over (order by chgRange, v desc) as rnk,
	char(10)+convert(varchar(10),DATEPART(WEEKDAY, closeDate - 1))+ ',' + convert(varchar(10), stockId)+'.TW' as stockId
	from vStock 
	where 1=1 and ma_dscr2='週↗月↗季↗年↗' and closeDate ='2020-08-04'
) as a where a.rnk<50
union
select stockid from 
(
	select 
	rank() over (order by chgRange, v desc) as rnk,
	'6,' + convert(varchar(10), stockId)+'.TW' as stockId
	from vStock 
	where 1=1 and closeDate ='2020-08-04' and isFavor=1
) as b where b.rnk<20
order by 1



update criticalPt set istouch=0 
delete from criticalPT where stockId=1325

select * from StockParas where closeDate='2020-07-21 00:00:00' order by stockId desc
select ma_dscr2,count(*) from vStock where closeDate='2020-07-24 00:00:00' group by  ma_dscr2 order by 2 desc 

select stockId,count(*) from vStock where closeDate='2020-07-21 00:00:00' group by stockId order by 2 desc

select * from StockParas where stockid='5474' order by closeDate desc
select * from vStock where stockid='5474' order by closeDate desc
select * from vStock where closeDate='2020-07-24 00:00:00' and ma_dscr2 ='週↗月↗季↗年↗'

select * from stock
update stock set isBeow=1 where id in (8403,8463,8440)

select 
stockid,
replace(
	replace(
		(select value as 'c' from string_split(dk_ma_dscr,',') 
		where isnumeric(value)=0
		for xml path(''))
	,'<c>','') 
,'</c>','') as ma_dscr2
,stockid,dk_ma_dscr
from vStock where closeDate='2020-07-06'




select closeDate,count(*) from volumn group by closeDate order by 1 desc
select closeDate,datename(dw,closeDate) as wk,count(*) from [dbo].[WkBT] group by closeDate

select * from [dbo].[WkBT] where closeDate='2020-08-03' order by stockId;
select stockId from [dbo].[WkBT] where closeDate='2020-07-21' group by stockId having count(*) >1;
select *,convert(varchar(10),createDate,120) from [dbo].[WkBT] where closeDate='2020-07-21' order by stockId

update wkbt set closeDate = convert(varchar(10),createDate,120) where closeDate='2020-07-21';


delete from [dbo].[WkBT] where closeDate='2020-06-18'

select len(stockid) from volumn order by 1 desc
select count(*) from volumn where closeDate='2020-06-17' 
select * from stock where id in (
select stockId from volumn where closeDate='2020-06-17'  group by stockId having count(*) =2 
)

--delete from volumn where closeDate='2020-06-17';

/*
找出三大法人的資料是否有重複? @20200621
*/
select closeDate,stockID,max(closeDate) as mx,min(closeDate) as minc, count(*) from volumn  group by closeDate,stockId having count(*) >1 order by 5,4 desc	
			drop table wrong_volumn;
			drop table right_volumn;

			select * into wrong_volumn from volumn where stockId in (
				select stockID from volumn  group by closeDate,stockId having count(*) >1 --先找出那些有問題的股票?
			)	
			select * from wrong_volumn where stockid =9917 and closeDate='2019-11-26'     --test!
			select stockId,closeDate,fc,ic,dealer,count(*) as cc into right_volumn  from wrong_volumn group by  stockId,closeDate,fc,ic,dealer; --排除重複的三大資料，寫入到正確的
			select * from right_volumn
			select stockId,closeDate,count(*) from right_volumn group by stockId,closeDate order by 3 desc
	delete from volumn where stockid in ( select stockId from right_volumn group by stockId );
	insert into volumn (stockId,closeDate,fc,ic,dealer)
			select stockId,closeDate,fc,ic,dealer from right_volumn;  --修改結束

select top 30 stockid,closeDate,fc,ic,dealer from vVolumn where stockid=1515 order by closeDate desc

select * from vVolumn
select * from vVolumn3Buy order by fc desc                                                      /* 今天三大法人同買的 */
select top 150 * from vLatestVolumn where summ > 100 order by fc desc,summ desc;
select top 50 * from vLatestVolumn order by ic desc
select top 50 * from vLatestVolumn order by dealer desc
select rank() over (order by fc desc),* from vLatestVolumn order by 1 
select * from stockLastCloseDate order by 1 desc




select ma_dscr2,count(*) from vStock where closeDate='2020-07-06' group by ma_dscr2 order by 2 desc
Select 
'6,'+convert(varchar(10),stockId)+'.TW',
kbar,stockid,stockName,
tag,o,v,amplitude
from vStock 
where 1=1 
and closeDate='2020-07-06' 
and ma_dscr2='週↘月↗季↗年↗' and iChk>0 
order by amplitude desc



--主要面板資訊------------------------------------------------------------------
declare @maxCloseDate as datetime;
select @maxCloseDate = max(closeDate) from stockLastCloseDate; 
--select @maxCloseDate
	--select count(*) as cc, max(closeDate) '最新日期',min(closeDate) as '最遠' from stockLastCloseDate;

--Rule 1：周五收盤，日K跳空很多的：叔叔的條件
select 
	top 50 
	--'11,'+convert(varchar(10),a.stockId)+'.TW' as yesId,
	a.stockID
	,a.stockName,tag, kbar,v,o,c,high,low,gap,iGap,amplitude,head,footer,wkma4
	,wkma4-low as d1
	,b.fc as '外資'
	from [vLatestStock] as a left join vVolumn3Buy as b on a.stockId =b.stockid and a.closeDate =b.closeDate
where 1=1
	--and gap = '向下跳空' 
	and kbar='紅K' 
	and c >= wkma4 and wkma4 <=low
	order by 
	--iGap desc,
	--amplitude desc,
	wkma4-low desc,
	head asc
	;

--Rule 2:季箱頂成功轉浪。
select 
'10,'+convert(varchar(10),stockId)+'.TW' as yesId ,
* from [vLatestStock] 
	where 1=1
		--and gap = '向上跳空' 
		and (wkdscr like '%年箱頂%' or wkdscr like '%季箱頂%') 
		and wkma13dt<>'2020-06-16 00:00:00' 
		and kbar='紅K' 
		and head=0
	order by amplitude desc
		
--Rule 2.1: 季箱頂
	select * from vLatestStock 
		where 1=1
		and isnull(dk_m_box,'')<>'' 
		and head =0 
		and (
			dk_ma_dscr like N'%週↗️%'
			  and dk_ma_dscr like N'%月↗️%'
			 -- and dk_ma_dscr like '%季↗️%' and
			  --dk_ma_dscr like '%年↗️%'
			  ) 
		order by amplitude desc

--Rule3：生達套路，大盤今天不好。
select * from [vLatestStock] where 1=1 
	 and dk_m_box like '%壓月次箱%' 
	 and wkdscr like '%下針壓過ma4均%'
	 and (dk_ma_dscr like '%週↗️%' and 
			  dk_ma_dscr like '%月↗️%' and
			  dk_ma_dscr like '%季↗️%' and
			  dk_ma_dscr like '%年↗️%') 
	 and dk_ma60 >= dk_ma240
 and dk_ma20 >= dk_ma60




select * from vLatestStock order by kbarStory desc


select * from stockParas where closeDate='2020-07-03'
select * from stockParas order by indication desc
select * from stockParas where cv_wma is not null

select min(closeDate) from stockParas
select * from WkBT where closeDate='2020-06-12' and stockid='2375'
delete from WkBT where id='7E6B7E8E-09F5-48DB-9667-CBB2CE0A7CDD'

---------------------------------------

select * from (
select a.stockId,b.name,a.kbar,a.c,a.l
,case 
	when kbar ='紅k' and o >= l then (o-l)/o * 100 
	when kbar ='綠k' and c >= l then (c-l)/c * 100
	else 0 end as [下針]
	,a.dscr
from WKBT as a join stock as b on a.stockId = b.id
where closeDate ='2020-06-11' and kbar='紅K'
) as t order by 下針 desc


select * from stockParas where stockID='5471' order by closeDate desc

select 'Group(1..12),Symbol' as ss union

select  top 10 * from vLatestStock

select
top 100 
char(10)+convert(varchar(10),DATEPART(WEEKDAY, a.closeDate-1))+ ',' + convert(varchar(10),a.stockId)+'.TW' as stockId
from vLatestStock as a
where a.c >= a.ma60 and a.c >= a.ma240 and cv_yma in (N'年↗',N'年－')
order by kbar,box_msg desc,isClean desc, amplitude desc


select
top 100 
--char(10)+convert(varchar(10),DATEPART(WEEKDAY, GETDATE()-1))+ ',' + convert(varchar(10),a.stockId)+'.TW' as stockId,
a.stockId,
a.name,
a.kbar,
a.isClean+a.predict,
a.box_msg,
cv_mma+'　'+cv_qma+'　'+cv_yma,
a.ma_alignment,
a.ma_tangle
from vLatestStock as a
where a.c >= a.ma60 and a.c >= a.ma240 and cv_yma in (N'年↗',N'年－')
order by kbar,box_msg desc,isClean desc, amplitude desc


select * from vLatestStock

select round(convert(decimal(2,1),4.55555),1)

select
top 100 
    char(10)+convert(varchar(10),a.stockId) as stockId
    ,a.name
    ,a.kbar+case when a.ma_alignment='' then '' else ','+a.ma_alignment end
    ,a.isClean+a.predict as predict
    ,a.box_msg+ case when a.ma_tangle='' then '' else ',' + a.ma_tangle end
    ,case when len(cv_mma)>2 then cv_mma else '' end + '　'
		+ case when len(cv_qma)>2 then cv_qma else '' end + '　'
		+ case when len(cv_yma)>2 then cv_yma else '' end       as cvma
	,convert(varchar(10),convert(decimal(3,1),amplitude))+'%' as 震幅
	,isnull(b.cc,'') as [營收新高]
from vLatestStock as a
	left join vMaxRevenue as b on a.stockId = b.stockId
where a.c >= a.ma60 and a.c >= a.ma240 and cv_yma in (N'年↗',N'年－')
order by kbar,box_msg desc,isClean desc, amplitude desc




update maxMMRevenue set diffMM = datediff(m,lastMaxDate, nowDate) where diffMM is null; 

select * from maxMMRevenue where nowDate='2020-06-10 00:00:00' order by 1 desc
--找出近十年 營收創新高
select stockId,stockName,cc from vMaxRevenue where nowDate='2020-06-10 00:00:00' order by cc desc

