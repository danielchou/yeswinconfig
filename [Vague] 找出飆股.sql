
truncate table stockParas;

select * from stock
select *from stockParas order by stockId
select * from vBestStock

Alter view vBestStock 
as
select 
b.id
,b.name
,a.v as '數量'
,a.o as '開盤'
,a.high as '收最高'
,a.amplitude as '震幅'
,a.ma20 as '月均'
,a.ma60 as '季均'
,a.ma240 As '年均'
,a.isClean As '預測開盤'
from stockParas as a 
join stock as b on a.stockId = b.id
where 
--a.isClean='明天續漲' and 
a.amplitude >7 and a.v>5000
order by a.amplitude desc


Exec sp_setKbar;

--目前有的筆數
select closeDate as 收盤日,count(*) as 出現基本訊號數量 from stockparas group by closeDate order by 1 desc

--昨天壓 今天又壓?
select * from stockParas where 1=1 
	and stockId in (select stockId from stockParas where closeDate='2020-04-27 00:00:00')
	and closeDate='2020-04-24 00:00:00'

--前天壓 昨天壓 今天又壓?
select 
'7,'+convert(varchar(10),b.id)+'.TW' as id,
b.name,
a.v 
	from stockParas as a join stock as b on a.stockId=b.id where 1=1
		and a.stockId in ( 
		select stockId from stockParas where 1=1 
			and stockId in (select stockId from stockParas where closeDate='2020-04-27 00:00:00')
			and closeDate='2020-04-24 00:00:00'
		) and a.closeDate='2020-04-23 00:00:00';

select * from stockParas where stockId='1515' order by closeDate 

--今天股價分析
declare @today as datetime; set @today='2020-04-28 00:00:00';
select * from vStock where closeDate=@today and box_msg<>''  and predict=N'近期短噴';

select yesid,name,kbar,v,cv_mma,cv_qma,cv_yma, 
	m_box,q_box,y_box,indication,amplitude,box_msg
	,ma_dscr,ma_alignment,ma_tangle,predict
	from vStock where closeDate=@today and box_msg<>'' and ma_alignment = N'多頭排列'

select kbar,count(*) 統計數 from stockParas where closeDate=@today group by kbar;

sp_helptext vStock

alter view vStock  
as   
select 
'7,'+convert(varchar(10),b.id)+'.TW' as yesId,
b.name,a.* from stockParas a left join stock b on a.stockId= b.id


select top 20 * from vstock where head =0 and closeDate ='2020/4/29 00:00:00' and y_box<>'' order by foot desc;

select 
closeDate,name,kbar,c as 收盤價,v as 數量,cv_mma,cv_qma,cv_yma, 
	m_box,q_box,y_box,indication,amplitude,box_msg
	,ma_dscr,ma_alignment,ma_tangle,predict
 from vstock where stockId='1515' order by closeDate desc;
